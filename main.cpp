#include <SDL.h>

#ifndef NULL
#define NULL 0
#endif//NULL

const Uint32 Cnegro = 0x0;
const Uint32 Cblanco = 0x00ffffff;
const Uint32 Cgris = 0x00303030;
const Uint32 Crojo = 0x00ff0000;
const Uint32 Cazul = 0x000000ff;
const Uint32 TamW = 640;
const Uint32 TamH = 480;
const Uint32 SepX = 10;
const Uint32 SepY = 10;

enum jugadores{
	ninguno,blanco,rojo,azul
};

inline void SDL_PutPixel(SDL_Surface *s, Uint32 x, Uint32 y, Uint32 c);
inline jugadores &Capturar(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY],Uint32 x, Uint32 y);
void maquina(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY]);
inline Uint32 lados(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY],Uint32 x,Uint32 y);
inline bool libre(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY],Uint32 x,Uint32 y, Uint32 &X, Uint32 &Y);

int main(int argc, char *argv[]){

	SDL_Surface *screen;
	Uint32 time = 0x0;
	SDL_Event evento;
	bool salida = false;
	jugadores hort[TamW/SepX][TamH/SepY];
	jugadores vert[TamW/SepX][TamH/SepY];
	Uint32 PosX,PosY;
	jugadores PosC;

	for(Uint32 i = 0; i < TamW/SepX; i++)
		for(Uint32 j = 0; j < TamH/SepY; j++){
			hort[i][j] = ninguno;
			vert[i][j] = ninguno;
	}

	SDL_InitSubSystem(SDL_INIT_VIDEO);
//	screen = SDL_SetVideoMode(TamW,TamH,32,SDL_HWSURFACE);
	SDL_Window *sdlWindow;
	SDL_Renderer *sdlRenderer;
	SDL_CreateWindowAndRenderer(TamW, TamH, 0, &sdlWindow, &sdlRenderer);
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
	SDL_RenderSetLogicalSize(sdlRenderer, TamW,TamH);
	SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
	SDL_Texture *sdlTexture = SDL_CreateTexture(sdlRenderer,
                               SDL_PIXELFORMAT_ARGB8888,
                               SDL_TEXTUREACCESS_STREAMING,
                               TamW,TamH);
	screen = SDL_CreateRGBSurface(0, TamW,TamH, 32,
                                        0x00FF0000,
                                        0x0000FF00,
                                        0x000000FF,
                                        0xFF000000);

	do{

		SDL_GetMouseState((int*)&PosX, (int*)&PosY);
		PosC = Capturar(hort,vert,PosX,PosY);
		Capturar(hort,vert,PosX,PosY) = blanco;
		SDL_FillRect(screen,NULL,Cnegro);
		SDL_LockSurface(screen);
		for(Uint32 i = 0; i < TamH; i+=SepY){
			Uint32 tmp = Cgris;
			for(Uint32 j = 0; j < TamW; j++){
				if(!(j%SepX))
					switch(hort[j/SepX][i/SepY]){
					case ninguno:
						tmp = Cgris;
						break;
					case blanco:
						tmp  = Cblanco;
						break;
					case rojo:
						tmp = Crojo;
						break;
					case azul:
						tmp = Cazul;
						break;
					default:;
				}
				SDL_PutPixel(screen,j,i,tmp);
			}
		}
		for(Uint32 i = 0; i < TamW; i+=SepX){
			Uint32 tmp = Cgris;
			for(Uint32 j = 0; j < TamH; j++){
				if(!(j%SepY))
					switch(vert[i/SepX][j/SepY]){
					case ninguno:
						tmp = Cgris;
						break;
					case blanco:
						tmp = Cblanco;
						break;
					case rojo:
						tmp = Crojo;
						break;
					case azul:
						tmp = Cazul;
						break;
					default:;
				}
				SDL_PutPixel(screen,i,j,tmp);
			}
		}
		SDL_UnlockSurface(screen);
//		SDL_Flip(screen);
		SDL_UpdateTexture(sdlTexture, NULL, screen->pixels, screen->pitch);
		SDL_RenderClear(sdlRenderer);
		SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, NULL);
		SDL_RenderPresent(sdlRenderer);

		Capturar(hort,vert,PosX,PosY) = PosC;

		if(SDL_PollEvent(&evento))
			switch(evento.type){
				jugadores tmp;
			case SDL_KEYDOWN:
				break;
			case SDL_MOUSEBUTTONDOWN:
				if(evento.button.button== SDL_BUTTON_LEFT)
					tmp = rojo;
				else
					tmp = azul;
				Capturar(hort,vert,evento.button.x,evento.button.y) = tmp;
				maquina(hort,vert);
				break;
			case SDL_QUIT:
				salida = true;
				break;
			default:;
		}


		if((time = SDL_GetTicks() - time) < 30)
			SDL_Delay(30 - time);
		time = SDL_GetTicks();
	}while(!salida);

	return 0;
}

inline void SDL_PutPixel(SDL_Surface *s, Uint32 x, Uint32 y,Uint32 c){

	*(Uint32*)((Uint8*)(s->pixels) + x*4 + y*s->pitch) = c;

	return;
}


inline jugadores &Capturar(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY],Uint32 x, Uint32 y){

	Uint32 tmpX = x/SepX;
	Uint32 tmpY = y/SepY;
	Uint32 tmp;
	Uint32 X = x%SepX, Y = y%SepY;

	/*
	if(X < SepX/2 )
		if(Y < SepY/2)
			if(X < Y)
				return vert[tmpX][tmpY];
			else
				return hort[tmpX][tmpY];
		else
			if(X < SepY-Y)
				return vert[tmpX][tmpY];
			else
				return hort[tmpX][++tmpY];
	else
		if(Y < SepY/2)
			if(SepX-X < Y)
				return vert[++tmpX][tmpY];
			else
				return hort[tmpX][tmpY];
		else
			if(SepX-X < SepY-Y)
				return vert[++tmpX][tmpY];
			else
				return hort[tmpX][++tmpY];
	*/
	
	return
	X<SepX/2?Y<SepY/2?X<Y?vert[tmpX][tmpY]:hort[tmpX][tmpY]:X<SepY-Y?vert[tmpX][tmpY]:hort[tmpX][++tmpY]:
	Y<SepY/2?SepX-X<Y?vert[++tmpX][tmpY]:hort[tmpX][tmpY]:SepX-X<SepY-Y?vert[++tmpX][tmpY]:hort[tmpX][++tmpY];
	
	
}


void maquina(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY]){

	Sint8 peso[2][TamW/SepX][TamH/SepY];
	for(Uint32 a = 0; a < 2; a++)
		for(Uint32 i = 0; i < TamW/SepX; i++)
			for(Uint32 j = 0; j < TamH/SepY; j++)
				peso[a][i][j] = 0;

	for(Uint32 i = 0; i < TamW/SepX; i++)
		for(Uint32 j = 0; j < TamH/SepY; j++){
			switch(lados(hort,vert,i,j)){
			case 0:
			case 1:
				break;
			}
	}

	return;
}


inline Uint32 lados(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY],Uint32 x,Uint32 y){

	if(x >= TamW || y >= TamH)
		throw "posicion fuera de rango\n";

	Uint32 cont = 0;

	if(hort[x][y] != ninguno)
		cont++;
	if(vert[x][y] != ninguno)
		cont++;
	if(hort[x][y+1] != ninguno)
		cont++;
	if(vert[x+1][y] != ninguno)
		cont++;

	return cont;
}


inline bool libre(jugadores (&hort)[TamW/SepX][TamH/SepY], jugadores (&vert)[TamW/SepX][TamH/SepY],Uint32 x,Uint32 y, Uint32 &X, Uint32 &Y){

	
	if(x >= TamW || y >= TamH)
		throw "posicion fuera de rango\n";

	X = x;
	Y = y;

	if(hort[x][y] != ninguno)
		return true;
	if(vert[x][y] != ninguno)
		return false;
	if(hort[x][y+1] != ninguno){
		Y++;return true;}
	if(vert[x+1][y] != ninguno){
		X++;return false;}
}
