# README #

This is a simple Dots and Boxes (https://en.wikipedia.org/wiki/Dots_and_Boxes) game written in SDL, originally was written at 2012 to SDL1.2, but has been ported to SDL2.

To use it, first you need to have installed http://libsdl.org/download-2.0.php under: /usr/local/include/SDL2 /usr/local/lib

Then just use: make ./main

THE GAME IS CURRENTLY UNDER CONSTRUCTION, so is not possible to play yet, for the moment just display the board highlight the line that mouse point, and mark that line with red or blue, when you click with the left or right button of the mouse respectively.